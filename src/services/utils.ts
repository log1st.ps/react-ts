import { CurrentUser } from '../models/CurrentUser'

export function formatNum(num: number) {
  return String(num).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1.')
}

export const getCookie = (name: string): string | null => {
  return (
    `; ${document.cookie}`.split(`; ${name}=`).pop()?.split(';')?.shift() ||
    null
  )
}

export const isAdmin = (permissions?: CurrentUser['permissions']) =>
  permissions
    ? permissions.forum.approveUnapprove ||
      permissions.forum.deleteAnyThread ||
      permissions.forum.lockUnlockThread ||
      permissions.forum.manageAnyThread ||
      permissions.forum.stickUnstickThread ||
      permissions.forum.undelete
    : false

export function formatDate(date: number) {
  return new Date(date).toLocaleDateString('ru', {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  })
}
