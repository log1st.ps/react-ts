export const ORIGIN = 'https://skladchik.ws' // document.location.protocol + '//' + document.location.host;

export const NEW_SKLADCHINA_ROUTE = '/new_skladchina'
export const isNewSkladchina = () =>
  window.location.pathname.includes(NEW_SKLADCHINA_ROUTE)

const endpoints: {
  [key in 'prod' | 'dev']: {
    [subKey in 'main' | 'new_sc']: {
      FETCH_THREADS_DATA: string
      FETCH_CATEGORIES_DATA: string
      FETCH_USER_DATA: string
      FETCH_SIDEBAR_DATA: string
      FETCH_CURRENT_USER: string
      ACTION_ON_THREADS: string
    }
  }
} = {
  prod: {
    main: {
      FETCH_THREADS_DATA: `${ORIGIN}/forumJx/forum`,
      FETCH_CATEGORIES_DATA: `${ORIGIN}/conversationApi2/thread/getNodesForUser`,
      FETCH_USER_DATA: `${ORIGIN}/members/memberJx`,
      FETCH_SIDEBAR_DATA: `${ORIGIN}/forumJx/sidebar`,
      FETCH_CURRENT_USER: `${ORIGIN}/conversationApi2/conversation/getInitUserData`,
      ACTION_ON_THREADS: `${ORIGIN}/inline-mod/thread/switchjx`,
    },
    new_sc: {
      FETCH_THREADS_DATA: `${ORIGIN}/shopping/newSkladchinaJx`,
      FETCH_CATEGORIES_DATA: `${ORIGIN}/conversationApi2/thread/getNodesForUser`,
      FETCH_USER_DATA: `${ORIGIN}/members/memberJx`,
      FETCH_SIDEBAR_DATA: `${ORIGIN}/forumJx/sidebar`,
      FETCH_CURRENT_USER: `${ORIGIN}/conversationApi2/conversation/getInitUserData`,
      ACTION_ON_THREADS: `${ORIGIN}/inline-mod/thread/switchjx`,
    },
  },
  dev: {
    main: {
      FETCH_THREADS_DATA: 'http://phalcon4.batikmsk.ru/forumJx/forum',
      FETCH_CATEGORIES_DATA:
        'http://phalcon4.batikmsk.ru/api/thread/getNodesForUser',
      FETCH_USER_DATA: 'http://phalcon4.batikmsk.ru/members/memberJx',
      FETCH_SIDEBAR_DATA: 'http://phalcon4.batikmsk.ru/forumJx/sidebar',
      FETCH_CURRENT_USER:
        'http://phalcon4.batikmsk.ru/api/conversation/getInitUserData',
      ACTION_ON_THREADS:
        'http://phalcon4.batikmsk.ru/inline-mod/thread/switchjx',
    },
    new_sc: {
      FETCH_THREADS_DATA: 'http://phalcon4.batikmsk.ru/newSkladchinaJx',
      FETCH_CATEGORIES_DATA:
        'http://phalcon4.batikmsk.ru/api/thread/getNodesForUser',
      FETCH_USER_DATA: 'http://phalcon4.batikmsk.ru/members/memberJx',
      // FETCH_COLLECT_SOON_DATA:
      //   'http://phalcon4.batikmsk.ru/api/shopping/getSoonCollecting',
      FETCH_SIDEBAR_DATA: 'http://phalcon4.batikmsk.ru/forumJx/sidebar',
      FETCH_CURRENT_USER:
        'http://phalcon4.batikmsk.ru/api/conversation/getInitUserData',
      ACTION_ON_THREADS:
        'http://phalcon4.batikmsk.ru/inline-mod/thread/switchjx',
    },
  },
}

export const isDev = false // document.location.host.includes('phalcon') || document.location.host.includes('localhost');

// eslint-disable-next-line no-nested-ternary
export const api = isDev
  ? isNewSkladchina()
    ? endpoints.dev.new_sc
    : endpoints.dev.main
  : isNewSkladchina()
  ? endpoints.prod.new_sc
  : endpoints.prod.main
