import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import store from './store'

import App from './App'
import { CoreContext } from './components/coreContext/CoreContext'
// import { getCookie } from './services/utils'

const rootNode = document.getElementById('threads-page')?.dataset?.nodeId

ReactDOM.render(
  <CoreContext.Provider
    value={{
      rootNode: rootNode ? +rootNode : null,
      xfSession: '77407,9b8107a3a19be368c5928133f93f4463bd7592dc',
    }}
  >
    <Provider store={store}>
      <App />
    </Provider>
  </CoreContext.Provider>,
  document.getElementById('threads-page')
)
