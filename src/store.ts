import { configureStore } from '@reduxjs/toolkit'
import { sidebarReducer } from './slices/sidebar/sidebarSlice'
import { categoriesReducer } from './slices/categories/categoriesSlice'
import { userReducer } from './slices/user/userSlice'
import { threadsReducer } from './slices/threads/threadsSlice'

const store = configureStore({
  reducer: {
    sidebar: sidebarReducer,
    categories: categoriesReducer,
    user: userReducer,
    threads: threadsReducer,
  },
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

export default store
