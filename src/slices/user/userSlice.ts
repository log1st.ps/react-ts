import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { CurrentUser } from '../../models/CurrentUser'
import { fetchUserDataRequest, fetchUserRequest } from './userAPI'

export interface UserState {
  isLoading: boolean
  user: CurrentUser | null
}

const initialState: UserState = {
  isLoading: false,
  user: null,
}

export const fetchUser = createAsyncThunk('user/fetch', fetchUserRequest)
export const fetchUserData = createAsyncThunk(
  'user/fetchData',
  fetchUserDataRequest
)

const slice = createSlice({
  name: 'user',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchUser.pending, (state) => {
        state.isLoading = true
      })
      .addCase(fetchUser.fulfilled, (state, payload) => {
        state.user = payload.payload
        state.isLoading = false
      })
      .addCase(fetchUser.rejected, (state) => {
        state.isLoading = false
      })
      .addCase(
        fetchUserData.fulfilled,
        (state, payload) => payload.payload as any
      )
  },
})

export const userReducer = slice.reducer
