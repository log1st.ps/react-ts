import { api } from '../../services/api'
import { CurrentUser } from '../../models/CurrentUser'
import { UserData } from '../../models/User'

export interface FetchUserParams {
  xfSession: string | null
}

export const fetchUserRequest = async (
  { xfSession }: FetchUserParams = { xfSession: null }
): Promise<CurrentUser> => {
  const url = new URL(api.FETCH_CURRENT_USER)
  if (xfSession) {
    url.search = new URLSearchParams({ xf_session: xfSession }).toString()
  }

  const response = await fetch(url.toString(), {
    method: 'GET',
  })
  return response.json()
}

export interface FetchUserDataParams {
  id: number
}

export const fetchUserDataRequest = async ({
  id,
}: FetchUserDataParams): Promise<UserData> => {
  const url = new URL(api.FETCH_USER_DATA)

  url.search = new URLSearchParams({
    user_id: String(id),
  }).toString()

  const response = await fetch(url.toString(), {
    method: 'GET',
  })
  return response.json()
}
