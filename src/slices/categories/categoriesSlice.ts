import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { fetchCategoriesRequest } from './categoriesAPI'
import { Category } from '../../models/Category'

export interface CategoriesState {
  isLoading: boolean
  categories: Array<Category>
}

const initialState: CategoriesState = {
  isLoading: false,
  categories: [],
}

export const fetchCategories = createAsyncThunk(
  'categories/fetch',
  fetchCategoriesRequest
)

const slice = createSlice({
  name: 'categories',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchCategories.pending, (state) => {
        state.isLoading = true
      })
      .addCase(fetchCategories.fulfilled, (state, payload) => {
        state.categories = payload.payload.data
        state.isLoading = false
      })
      .addCase(fetchCategories.rejected, (state) => {
        state.isLoading = false
      })
  },
})

export const categoriesReducer = slice.reducer
