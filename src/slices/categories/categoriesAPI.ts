import { CategoriesState } from './categoriesSlice'
import { api } from '../../services/api'

export interface FetchCategoriesParams {
  parentId: null | number
}

export const fetchCategoriesRequest = async (
  { parentId }: FetchCategoriesParams = { parentId: null }
): Promise<{
  data: CategoriesState['categories']
}> => {
  const url = new URL(api.FETCH_CATEGORIES_DATA)
  if (parentId) {
    url.search = new URLSearchParams({ parentId: String(parentId) }).toString()
  }

  const response = await fetch(url.toString(), {
    method: 'GET',
  })
  return response.json()
}
