import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { fetchThreadsRequest } from './threadsAPI'
import { Thread } from '../../models/Thread'
import { Category } from '../../models/Category'

export enum ThreadsDisplayType {
  list = 'list',
  grid = 'grid',
  table = 'table',
}

export interface ThreadsState {
  isLoading: boolean
  settings: {
    displayType: ThreadsDisplayType
    selectedThreads: Array<Thread['thread_id']>
  }
  filters: {
    text: string
    sortThreads: null | 'asc' | 'desc'

    isOrg: 0 | 1 | 2
    isPart: 0 | 1 | 2
    imgPreview: 0 | 1 | 2
    deleted: 0 | 1 | 2 | 3
    sortField: null | string
    sortDirection: null | 'asc' | 'desc'
    selectedCategories: Array<Category['id']>
    stages: number[]

    from: number | null
    to: number | null

    page: number
  }
  data: {
    page: number
    totalThreads: number
    totalPages: number
    threadsPerPage: number
    threads: Array<Thread>
    stickyThreads: Array<Thread>
  }
}

const initialState: ThreadsState = {
  isLoading: false,
  filters: {
    text: '',
    sortThreads: null,
    isOrg: 0,
    isPart: 0,
    imgPreview: 0,
    deleted: 0,
    sortField: 'lastPostDate',
    sortDirection: 'desc',
    selectedCategories: [],
    from: null,
    to: null,
    stages: [],
    page: 1,
  },
  data: {
    page: 1,
    totalThreads: 0,
    totalPages: 0,
    threadsPerPage: 0,
    threads: [],
    stickyThreads: [],
  },
  settings: {
    displayType: ThreadsDisplayType.table,
    selectedThreads: [],
  },
}

export const fetchThreads = createAsyncThunk(
  'threads/fetch',
  // @ts-ignore
  fetchThreadsRequest
)

const slice = createSlice({
  name: 'threads',
  initialState,
  reducers: {
    setFilterCategories: (
      state,
      payload: PayloadAction<Array<Category['id']>>
    ) => {
      state.filters.selectedCategories = payload.payload
    },
    setFilterText: (
      state,
      payload: PayloadAction<ThreadsState['filters']['text']>
    ) => {
      state.filters.text = payload.payload
    },
    setFilterFrom: (
      state,
      payload: PayloadAction<ThreadsState['filters']['from']>
    ) => {
      state.filters.from = payload.payload
    },
    setFilterTo: (
      state,
      payload: PayloadAction<ThreadsState['filters']['to']>
    ) => {
      state.filters.to = payload.payload
    },
    setFilterIsOrg: (
      state,
      payload: PayloadAction<ThreadsState['filters']['isOrg']>
    ) => {
      state.filters.isOrg = payload.payload
    },
    setFilterIsPart: (
      state,
      payload: PayloadAction<ThreadsState['filters']['isPart']>
    ) => {
      state.filters.isPart = payload.payload
    },
    setFilterImgPreview: (
      state,
      payload: PayloadAction<ThreadsState['filters']['imgPreview']>
    ) => {
      state.filters.imgPreview = payload.payload
    },
    setFilterDeleted: (
      state,
      payload: PayloadAction<ThreadsState['filters']['deleted']>
    ) => {
      state.filters.deleted = payload.payload
    },
    setFilterSortThreads: (
      state,
      payload: PayloadAction<ThreadsState['filters']['sortThreads']>
    ) => {
      state.filters.sortThreads = payload.payload
    },
    setFilterStages: (
      state,
      payload: PayloadAction<ThreadsState['filters']['stages']>
    ) => {
      state.filters.stages = payload.payload
    },
    setFilterPage: (
      state,
      payload: PayloadAction<ThreadsState['filters']['page']>
    ) => {
      state.filters.page = payload.payload
    },
    setFilterSortField: (
      state,
      payload: PayloadAction<ThreadsState['filters']['sortField']>
    ) => {
      state.filters.sortField = payload.payload
    },
    setFilterSortDirection: (
      state,
      payload: PayloadAction<ThreadsState['filters']['sortDirection']>
    ) => {
      state.filters.sortDirection = payload.payload
    },
    setSettingsDisplayType: (
      state,
      payload: PayloadAction<ThreadsState['settings']['displayType']>
    ) => {
      state.settings.displayType = payload.payload
    },
    setSettingsSelectedThreads: (
      state,
      payload: PayloadAction<ThreadsState['settings']['selectedThreads']>
    ) => {
      state.settings.selectedThreads = payload.payload
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchThreads.pending, (state) => {
        state.isLoading = true
      })
      .addCase(fetchThreads.fulfilled, (state, payload) => {
        state.isLoading = false
        state.data = payload.payload
      })
      .addCase(fetchThreads.rejected, (state) => {
        state.isLoading = false
      })
  },
})

export const threadsReducer = slice.reducer
export const {
  setFilterCategories,
  setFilterText,
  setFilterFrom,
  setFilterTo,
  setFilterIsOrg,
  setFilterIsPart,
  setFilterImgPreview,
  setFilterDeleted,
  setFilterSortThreads,
  setFilterStages,
  setFilterPage,
  setSettingsDisplayType,
  setFilterSortField,
  setFilterSortDirection,
  setSettingsSelectedThreads,
} = slice.actions
