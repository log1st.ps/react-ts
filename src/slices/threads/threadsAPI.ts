import { api } from '../../services/api'
import { RootState } from '../../store'
import { ThreadsState } from './threadsSlice'

export interface FetchThreadsParams {
  parentId: null | number
}

export const fetchThreadsRequest = async (
  { parentId }: FetchThreadsParams = { parentId: null },
  thunkAPI: { getState: () => RootState }
): Promise<ThreadsState['data']> => {
  const url = new URL(api.FETCH_THREADS_DATA)
  const { filters } = thunkAPI.getState().threads

  url.search = new URLSearchParams(
    Object.entries({
      ...filters,
      parentId,
    }).reduce(
      (acc, [field, value]) => ({
        ...acc,
        ...(['', null, undefined].includes(value as any)
          ? {}
          : {
              [field]: value,
            }),
      }),
      {}
    ) as any
  ).toString()

  const response = await fetch(url.toString(), {
    method: 'GET',
  })
  return response.json()
}
