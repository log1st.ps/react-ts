import { SidebarState } from './sidebarSlice'
import { api } from '../../services/api'
import { OnlineRecord } from '../../models/Online'

const uniqueUserByIdFilter = (
  user: OnlineRecord,
  index: number,
  self: Array<OnlineRecord>
) => self.findIndex(({ user_id }) => user_id === user.user_id) === index

export const fetchSidebarRequest = async <
  T extends Omit<SidebarState, 'isLoading'>
>(): Promise<T> => {
  const response = await fetch(api.FETCH_SIDEBAR_DATA, {
    method: 'GET',
  })
  const {
    onlineStaff,
    online60,
    online15,
    ...etc
  } = (await response.json()) as T
  return {
    ...etc,
    online60: {
      ...online60,
      records: online60.records.filter(uniqueUserByIdFilter),
    },
    online15: {
      ...online15,
      records: online15.records.filter(uniqueUserByIdFilter),
    },
    onlineStaff: {
      ...onlineStaff,
      records: onlineStaff.records.filter(uniqueUserByIdFilter),
    },
  } as T
}
