import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { User } from '../../models/User'
import { OnlineRecord, SoonCollection } from '../../models/Online'
import { fetchSidebarRequest } from './sidebarAPI'

export interface SidebarOnlineStateSlice {
  guests: number
  robots: number
  members: number
  total: number
  records: Array<OnlineRecord>
}

export interface SidebarState {
  isLoading: boolean
  boardTotals: {
    discussions: number
    messages: number
    latestUser: User | null
    users: number
  }
  online60: SidebarOnlineStateSlice
  online15: SidebarOnlineStateSlice
  onlineStaff: SidebarOnlineStateSlice
  collectionSoon: Array<SoonCollection>
}

const getOnlineSlice = (): SidebarOnlineStateSlice => ({
  guests: 0,
  robots: 0,
  members: 0,
  total: 0,
  records: [],
})

const initialState: SidebarState = {
  isLoading: false,
  boardTotals: { discussions: 0, latestUser: null, messages: 0, users: 0 },
  collectionSoon: [],
  online15: getOnlineSlice(),
  online60: getOnlineSlice(),
  onlineStaff: getOnlineSlice(),
}

export const fetchSidebar = createAsyncThunk<Omit<SidebarState, 'isLoading'>>(
  'sidebar/fetch',
  fetchSidebarRequest
)

const slice = createSlice({
  name: 'sidebar',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchSidebar.pending, (state) => {
        state.isLoading = true
      })
      .addCase(fetchSidebar.fulfilled, (state, payload) => {
        state.online60 = payload.payload.online60
        state.online15 = payload.payload.online15
        state.onlineStaff = payload.payload.onlineStaff
        state.collectionSoon = payload.payload.collectionSoon
        state.boardTotals = payload.payload.boardTotals
        state.isLoading = false
      })
      .addCase(fetchSidebar.rejected, (state) => {
        state.isLoading = false
      })
  },
})

export const sidebarReducer = slice.reducer
