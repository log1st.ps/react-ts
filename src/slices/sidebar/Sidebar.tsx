import React, { useEffect } from 'react'
import { fetchSidebar } from './sidebarSlice'
import { useAppDispatch } from '../../hooks/store'
import { SupportLinks } from '../../components/supportLinks/SupportLinks'
import BoardTotals from '../../components/boardTotals/BoardTotals'

const Sidebar: React.FC = () => {
  const dispatch = useAppDispatch()
  useEffect(() => {
    dispatch(fetchSidebar())
  }, [fetchSidebar, dispatch])

  return (
    <>
      <SupportLinks />
      <BoardTotals />
    </>
  )
}

export default Sidebar
