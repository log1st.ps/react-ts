import { Language } from './Language'
import { Style } from './Style'
import { UserGroup } from './UserGroup'
import { DbBoolean } from './Helpers'

export enum UserGender {
  male = 'male',
}

export enum UserState {
  valid = 'valid',
}

export interface UserData {
  canBanUsers: boolean
  canCleanSpam: boolean
  canEditUser: boolean
  canIgnore: boolean
  canStartConversation: boolean
  canViewCurrentActivity: true
  canViewIps: boolean
  canViewOnlineStatus: true
  canViewWarnings: boolean
  canWarn: boolean
  user: any;
}

export interface User {
  user_id: number
  username: string
  email: string
  gender: UserGender
  custom_title: string
  language_id: Language['id']
  style_id: Style['id']
  timezone: string
  visible: DbBoolean
  activity_visible: DbBoolean
  user_group_id: UserGroup['id']
  secondary_group_ids: string
  display_style_group_id: 2
  permission_combination_id: 2
  message_count: number
  conversations_unread: number
  conversations_unread_old: number
  register_date: number
  last_activity: number
  trophy_points: number
  alerts_unread: number
  alerts_unread_support: number
  avatar_date: number
  avatar_width: number
  avatar_height: number
  gravatar: string
  user_state: UserState
  is_moderator: DbBoolean
  is_admin: DbBoolean
  is_banned: DbBoolean
  like_count: number
  warning_points: number
  is_staff: DbBoolean
  old_group: null
  old_group_update_status: 0
  multiacctoken: string
  brsts_support_ticket_unread: number
  brsts_active_ticket_department: number
  brsts_ticket_count: number
  brsts_department_ids: string
  payment_count: number
  organized_count: number
  email_error: DbBoolean
  email_ban: DbBoolean
  recive_tips: number
  current_tip: number
  recive_news: number
  last_tip_time: number
  telegram_id: null | string
  source_id: number
}
