export enum CategoryNodeTypeID {
  Forum = 'Forum',
}

export interface Category {
  id: number
  displayOrder: number
  hasSubCategories: boolean
  link: string
  nodeTypeId: CategoryNodeTypeID
  parentId: Category['id']
  subCategories: Array<Category>
  title: string
  left: number
  top: number
}
