import { DbBoolean } from './Helpers'
import { UserGroup } from './UserGroup'

import partnerIcon from '../assets/p.png'
import standardIcon from '../assets/std.png'

export interface ThreadStage {
  id: number
  name: string
  nameRu: string
}

export const shoppingTypes = {
  1: {
    icon: standardIcon,
    text: 'Стандартная складчина',
  },
  2: {
    icon: partnerIcon,
    text: 'Партнерская складчина',
  },
}

export const threadStages: Array<ThreadStage> = [
  {
    id: 1,
    name: 'collect-soon',
    nameRu: 'Скоро сбор взносов',
  },
  {
    id: 2,
    name: 'open',
    nameRu: 'Запись',
  },
  {
    id: 3,
    name: 'active',
    nameRu: 'Сбор взносов',
  },
  {
    id: 4,
    name: 'finished',
    nameRu: 'Доступно',
  },
  {
    id: 5,
    name: 'closed',
    nameRu: 'Завершено',
  },
]

export interface Thread {
  thread_id: number
  node_id: number
  title: string
  reply_count: number
  view_count: number
  user_id: number
  username: string
  post_date: number
  sticky: DbBoolean
  discussion_state: string
  discussion_open: DbBoolean
  discussion_type: string
  first_post_id: number
  first_post_likes: number
  last_post_date: number
  last_post_id: number
  last_post_user_id: number
  last_post_username: string
  last_post_link: string
  prefix_id: number
  hide: DbBoolean
  up_time: number
  tab_title: string
  tab_content: null
  tab_link: string
  tab_link_desc: string
  tags: string
  similar_threads: string
  title_back: null
  min_pays_count: number
  node_title: string
  node_name: null
  email: string
  gender: string
  custom_title: string
  language_id: number
  style_id: number
  timezone: string
  visible: DbBoolean
  activity_visible: DbBoolean
  user_group_id: UserGroup['id']
  secondary_group_ids: string
  display_style_group_id: number
  permission_combination_id: number
  message_count: number
  conversations_unread: number
  conversations_unread_old: number
  register_date: number
  last_activity: number
  trophy_points: number
  alerts_unread: number
  alerts_unread_support: number
  avatar_date: number
  avatar_width: number
  avatar_height: number
  gravatar: string
  user_state: string
  is_moderator: DbBoolean
  is_admin: DbBoolean
  is_banned: DbBoolean
  like_count: number
  warning_points: number
  is_staff: DbBoolean
  old_group: null | number
  old_group_update_status: number
  multiacctoken: string
  brsts_support_ticket_unread: number
  brsts_active_ticket_department: number
  brsts_ticket_count: number
  brsts_department_ids: string
  payment_count: number
  organized_count: number
  email_error: DbBoolean
  email_ban: DbBoolean
  recive_tips: number
  current_tip: number
  recive_news: number
  last_tip_time: number
  telegram_id: null
  source_id: number
  shopping_id: number
  shopping_price: number
  shopping_participants: number
  deadline_shopping: null
  shopping_type_id: number
  optimized_payment: DbBoolean
  shopping_stage: string
  organizer_id: number
  organizer_username: string
  message: string
  message_html: string
  last_post_user_gravatar: string
  last_post_avatar_date: number
  last_post_display_style_group_id: number
  last_post_user_group_id: number
  shopping_participants_current: number
  participants_count: number
  thread_read_date: null
  thread_is_watched: DbBoolean
  user_post_count: number
  main_image: null
  forum: {
    node_id: number
    title: string
    node_name: null
  }
  canInlineMod: DbBoolean
  canEditThread: DbBoolean
  isNew: DbBoolean
  haveReadData: DbBoolean
  hasPreview: DbBoolean
  canViewContent: DbBoolean
  isRedirect: DbBoolean
  isDeleted: DbBoolean
  isModerated: DbBoolean
  titleCensored: DbBoolean
  lastPageNumbers: boolean
  lastPostInfo: {
    post_date: number
    post_id: number
    user_id: number
    username: string
    isIgnoring: boolean
    gravatar: string
    display_style_group_id: number
  }
  customFields: []
  externalAuth: []
  isTrusted: boolean
  isIgnored: boolean
  tagsList: {
    [key: string]: {
      tag: string
      tag_url: string
    }
  }
  thread_link: string
  user_link: string
  icon_keys: Array<{ className: string; text: string }>
  post_image: string
  last_post_user_avatar: string
  last_post_user_style: string
  last_post_user_link: string
}
