import { DbBoolean } from './Helpers'

export interface CurrentUser {
  userId: number
  username: string
  timezone: string
  avatar: string
  permissions: {
    canStartConversation: DbBoolean
    canSeeOrgFeed: DbBoolean
    CustomTabInThreads: {
      addCustomTab: boolean
      adminReviews: boolean
      editOwnThreadCustomTab: boolean
      editAnyThreadCustomTab: boolean
    }
    avatar: { allowed: boolean; maxFileSize: number }
    general: {
      canSeeModerFilters: boolean
      canSeeUserFilters: boolean
      canUseKeywordFilters: boolean
      editSignature: boolean
      view: boolean
      viewMemberList15: boolean
      viewNode: boolean
      viewIps: boolean
      viewMemberList: boolean
      bypassUserPrivacy: boolean
      viewProfile: boolean
      search: boolean
      cleanSpam: boolean
      maxTaggedUsers: number
      createTag: boolean
      bypassUserTagLimit: boolean
      viewWarning: boolean
      warn: boolean
      manageWarning: boolean
      editBasicProfile: boolean
      editProfile: boolean
      editCustomTitle: boolean
      requireTfa: boolean
      followModerationRules: boolean
      bypassFloodCheck: boolean
      bypassSpamCheck: boolean
      report: boolean
      can_post_with_url: boolean
      TPUSpamRegModAllPosts: boolean
      can_view_mod_conv: boolean
    }
    forum: {
      can_create_new_nodes: boolean
      can_remove_on_domain: boolean
      can_see_user_debt: boolean
      viewOthers: boolean
      viewContent: boolean
      like: boolean
      postThread: boolean
      postReply: boolean
      stickUnstickThread: boolean
      deleteOwnPost: boolean
      editOwnPost: boolean
      lockUnlockThread: boolean
      editOwnPostTimeLimit: number
      editOwnThreadTitle: boolean
      deleteOwnThread: boolean
      manageAnyThread: boolean
      viewAttachment: boolean
      uploadAttachment: boolean
      tagOwnThread: boolean
      tagAnyThread: boolean
      manageOthersTagsOwnThread: boolean
      deleteAnyThread: boolean
      hardDeleteAnyThread: boolean
      threadReplyBan: boolean
      votePoll: boolean
      editAnyPost: boolean
      deleteAnyPost: boolean
      hardDeleteAnyPost: boolean
      warn: boolean
      manageAnyTag: boolean
      viewDeleted: boolean
      viewModerated: boolean
      undelete: boolean
      approveUnapprove: boolean
    }
    UserBanByModers: { can_forever_ban: boolean; ak_can_ban: DbBoolean }
    ReadConversation: {
      can_payment_conversation: boolean
      can_delivery_conversation: boolean
      can_other_conversation: boolean
    }
    HideSomeThreads: { can_sort_org: boolean; can_sort_participant: boolean }
    siropu_username_change: {
      change: boolean
      incognito: boolean
      history: boolean
      changeLimit: DbBoolean
      timeFrame: DbBoolean
    }
    changeAuthorGroupID: { changeAuthorID: boolean }
    changeDateGroupID: { changeDateID: boolean }
    estcs: {
      fixed_award_max: number
      fixed_award_min: number
      percent_for_org_fee: number
      price_of_product: number
      percent_reserve: number
      prc_rsrv_after_estimate: number
      author_main_interest: number
      author_percentage_reserve: number
      author_percent_addit: number
      can_view_referals: boolean
      write_about_and_country: boolean
      no_calc: boolean
      can_mark_as_paid: boolean
      can_stage_finished: boolean
      can_see_shopping_type: boolean
      can_downgrade: boolean
      can_mark_as_not_paid: boolean
      change_email_receive_mes: boolean
      hide_activity: boolean
      hide_status: boolean
      write_mes_in_status: boolean
      estcs_can_create: boolean
      estcs_can_manage: boolean
      estcs_can_organize: boolean
      can_see_link_atcard_org: boolean
      estcs_can_approve_org: boolean
      can_see_link_atcard_join: boolean
      estcs_can_see: boolean
      estcs_see_organized_user: boolean
      estcs_can_join_primary: boolean
      estcs_organizer_limit: number
      estcs_can_edit_payment: boolean
      estcs_can_join_reserve: boolean
      estcs_can_join_anonymous: boolean
      estcs_moderator_confirm: boolean
      estcs_can_join_additional: boolean
      estcs_users_confirm: boolean
      estcs_can_see_bugalteria: boolean
      estcs_can_get_product: boolean
      estcs_can_get_money: boolean
      estcs_can_get_reserve: boolean
      estcs_can_send_incomplete: boolean
      can_edit_shopping_at_stag: boolean
      estcs_fee_type_id: number
      estcs_fee_fix_ratio: number
      estcs_fee_percent_ratio: number
      estcs_estimate_thread: number
      estcs_forum_fee_type_id: number
      shopping_type_author: boolean
      shopping_type_partner: boolean
      can_invisible_shopp: boolean
    }
    liam_loginAsUser: {
      liam_loginAsUser_use: boolean
      liam_loginAsUser_br: boolean
      liam_loginAsUser_sc: boolean
    }
    memberNotesGroupID: { memberNotesID: boolean }
    BR_SupportTicket: {
      useTicket: boolean
      openSupportTicket: boolean
      viewAnyTicket: boolean
      manageAnyTicket: boolean
      uploadAttachment: boolean
      editAnyTicket: boolean
      deleteTicket: boolean
      editTicketStatus: boolean
      editTicketDepartment: boolean
      assignSupportTicket: boolean
      changeOwner: boolean
      viewAssigned: boolean
      openTicketAnyone: boolean
      viewSensitive: boolean
      viewSubmitterIp: boolean
      usePredefinedReply: boolean
      moveTicketForum: boolean
      deleteAnyMessage: boolean
      hardDeleteAnyMessage: boolean
      viewCurrentViewer: boolean
      viewDeleted: boolean
      editAnyMessage: boolean
      editOwnMessage: boolean
      deleteOwnMessage: boolean
      maxTaggedUsers: number
    }
    sklSklmadMultiAccInfo: {
      sklSmaiIgnoreMe: boolean
      sklSmaiSeeAllProfileInfo: boolean
      sklSmaiSeeOwnProfileInfo: boolean
    }
    wmtStickyMultiAccInfo: {
      wmtSmaiIgnoreMe: boolean
      wmtSmaiSeeAllProfileInfo: boolean
      wmtSmaiSeeOwnProfileInfo: boolean
      wmtSmaiSeeExemptions: boolean
      wmtSmaiManageExemptions: boolean
    }
    signature: {
      basicText: boolean
      extendedText: boolean
      align: boolean
      list: boolean
      image: boolean
      link: boolean
      media: boolean
      block: boolean
      maxPrintable: number
      maxLines: number
      maxLinks: number
      maxImages: number
      maxSmilies: number
      maxTextSize: number
    }
    profilePost: {
      editAny: boolean
      view: boolean
      like: boolean
      deleteAny: boolean
      manageOwn: boolean
      hardDeleteAny: boolean
      post: boolean
      warn: boolean
      comment: boolean
      deleteOwn: boolean
      editOwn: boolean
      viewDeleted: boolean
      viewModerated: boolean
      undelete: boolean
      approveUnapprove: boolean
    }
    conversation: {
      editAnyPost: boolean
      start: boolean
      receive: boolean
      alwaysInvite: boolean
      convess_editOwnConvo: boolean
      uploadAttachment: boolean
      convess_editAnyConvo: boolean
      editOwnPost: boolean
      editOwnPostTimeLimit: number
      convessDeleteOwnTimeLimit: number
      maxRecipients: number
      viewAny: boolean
      can_old_conversations: boolean
      convessInboxSize: number
      convessInboxMessages: number
      convessParticipantGroups: boolean
      convessAutoResponse: boolean
      convessPrefixes: boolean
      convessFilters: boolean
      convessKickRecipients: boolean
      convessLikeMessages: boolean
      convessCopyToThread: boolean
    }
    estebbc: {
      estebbc_hide_can_view_all: boolean
      estebbc_limit_posts: number
      estebbc_limit_likes: number
      estebbc_limit_trophies: number
      estebbc_limit_days: number
    }
    tlk_readpc: { permission_check: boolean; delete_pc: boolean }
    BRSTS_KnowledgeBase: {
      viewKnowledge: boolean
      rateKnowledge: boolean
      createKnowledgeBase: boolean
      editAnyKb: boolean
      deleteAnyKb: boolean
    }
  }
  answersCount: { dialog: []; payment: []; library: [] }
  payInformCount: number
  userRole: []
  navCounters: {
    myThemes: number
    organizedThemes: number
    unreadConversation: number
    unreadGroupConversation: number
    alerts: number
    alertsSupport: number
  }
  token: string
}
