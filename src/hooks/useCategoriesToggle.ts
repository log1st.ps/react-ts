import { useCallback, useState } from 'react'
import { Category } from '../models/Category'

const getSubIds = (categories: Array<Category>): Array<Category['id']> =>
  categories.reduce(
    (acc, cur) => [
      ...acc,
      cur.id,
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      ...(cur.hasSubCategories ? getSubIds(cur.subCategories) : []),
    ],
    [] as Array<Category['id']>
  )

export const useCategoriesToggle = <
  T extends {
    id: Category['id']
    subCategories: Category['subCategories']
  }
>(
  initialValue: Array<T['id']>,
  selectSubCategories = false
): [Array<T['id']>, (id: T['id']) => boolean, (item: T) => void] => {
  const [value, setValue] = useState<Array<T['id']>>(initialValue)
  const get = useCallback((id: T['id']) => value.includes(id), [
    value,
    selectSubCategories,
  ])

  const toggle = useCallback(
    ({
      id,
      subCategories,
    }: {
      id: T['id']
      subCategories?: T['subCategories']
    }) => {
      const index = value.indexOf(id)
      const subIds = selectSubCategories ? getSubIds(subCategories || []) : []
      if (index === -1) {
        setValue([...value, id, ...subIds])
      } else {
        const newExpanded = [...value]
        newExpanded.splice(index, 1)
        setValue(newExpanded.filter((i) => !subIds.includes(i)))
      }
    },
    [value, selectSubCategories]
  )

  return [value, get, toggle]
}
