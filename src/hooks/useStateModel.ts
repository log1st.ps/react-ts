import { ChangeEvent, useCallback, useEffect, useState } from 'react'
import { PayloadActionCreator } from '@reduxjs/toolkit/src/createAction'
import { RootState } from '../store'
import { useAppDispatch, useAppSelector } from './store'

export const useStateModel = <Selected, ToUpdate = Selected>(
  getter: (state: RootState) => Selected,
  setter: PayloadActionCreator<ToUpdate>,
  emitOnChange: boolean = false,
  changer?: (event: any) => Selected,
  emptyValue: any = null
): [
  Selected,
  (value: Selected, emit?: boolean) => void,
  () => void,
  (e: any) => void,
  () => void
] => {
  const stateValue = useAppSelector(getter)
  const dispatch = useAppDispatch()
  const [value, setValueDefault] = useState(stateValue)

  const emit = useCallback(() => {
    dispatch(setter(value))
  }, [setter, value])

  const setValue = useCallback(
    (val: Selected, emitImmediately = false) => {
      setValueDefault(val)
      if (!emitOnChange && emitImmediately) {
        dispatch(setter(val))
      }
    },
    [setValueDefault, setter]
  )

  const onChange = useCallback(
    (event: ChangeEvent<any>) => {
      setValue(
        changer
          ? changer((event.currentTarget.value as unknown) as any)
          : event.currentTarget.value
      )
    },
    [setValue, changer]
  )

  const onClear = useCallback(() => {
    setValue(emptyValue)
  }, [setValue, emptyValue])

  useEffect(() => {
    if (!emitOnChange) {
      return
    }
    emit()
  }, [emitOnChange, value, emit])

  useEffect(() => {
    setValue(stateValue)
  }, [setValue, stateValue])

  return [value, setValue, emit, onChange, onClear]
}
