import React, { useMemo } from 'react'
import { useAppSelector } from '../../hooks/store'
import { ORIGIN } from '../../services/api'

export const MakeThread: React.FC = () => {
  const selectedIds = useAppSelector(
    (state) => state.threads.filters.selectedCategories
  )

  const lastSelectedId = useMemo(() => [...selectedIds].pop() || 31, [
    selectedIds,
  ])

  const url = useMemo(
    () => `${ORIGIN}/razdel.${lastSelectedId}/create-shopping-thread`,
    [lastSelectedId]
  )

  return (
    <a href={url} className="callToAction clear-app__make-thread">
      <span>Создать тему</span>
    </a>
  )
}
