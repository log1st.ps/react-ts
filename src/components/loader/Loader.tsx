import React from 'react'
import { isDev } from '../../services/api'

const ThreadLoading = () => {
  return (
    <img
      src={
        isDev
          ? '/threads-loader.gif'
          : '/js/react/main-page-threads-filter/media/threads-loader.gif'
      }
      alt="Loading"
      id="threads-loader"
    />
  )
}

export default ThreadLoading
