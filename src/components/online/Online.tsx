import React from 'react'
import { Username } from '../username/Username'
import { SidebarOnlineStateSlice } from '../../slices/sidebar/sidebarSlice'

export interface OnlineProps {
  data: SidebarOnlineStateSlice
}

export const Online: React.FC<OnlineProps> = ({ data }) => {
  return (
    <>
      <div className="section userList">
        <div className="secondaryContent">
          <h3>
            <a href="/online" title="Показать всех пользователей, кто онлайн">
              Пользователи онлайн
            </a>
          </h3>
          <ol className="listInline">
            {data.records.map((record) => (
              <li key={record.user_id}>
                <Username username={record.username} id={record.user_id} />
              </li>
            ))}
          </ol>
          <div className="footnote">
            {`Всего: ${data.total} (пользователей: ${data.members}, гостей: ${data.guests}, роботов: ${data.robots})`}
          </div>
        </div>
      </div>
    </>
  )
}
