import React, { useCallback } from 'react'
import classNames from 'classnames'
import { useStateModel } from '../../hooks/useStateModel'
import { setFilterStages } from '../../slices/threads/threadsSlice'
import { threadStages } from '../../models/Thread'

export const StagesTabs: React.FC = () => {
  const [stages, , , onStagesChange] = useStateModel(
    (state) => state.threads.filters.stages,
    setFilterStages,
    true
  )

  const onAllStageClick = useCallback(() => {
    onStagesChange({ currentTarget: { value: [] } })
  }, [setFilterStages])

  const getStageClick = useCallback(
    (stage: number) => () => {
      onStagesChange({ currentTarget: { value: [stage] } })
    },
    [onStagesChange]
  )

  return (
    <ul className="tabs">
      <li
        className={classNames({
          active: stages.length === 0,
        })}
        onClick={onAllStageClick}
      >
        <span>Все</span>
      </li>
      {threadStages.map(({ id, nameRu }) => (
        <li
          className={classNames({
            active: stages.includes(id),
          })}
          onClick={getStageClick(id)}
          key={id}
        >
          <span>{nameRu}</span>
        </li>
      ))}
    </ul>
  )
}
