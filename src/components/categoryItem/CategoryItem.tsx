import React, { useCallback, useContext } from 'react'
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Checkbox,
  FormControlLabel,
} from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import { Category } from '../../models/Category'
import { useAppSelector } from '../../hooks/store'
import { CategoriesContext } from '../categories/Categories'

export interface CategoryItemProps
  extends Pick<
    Category,
    'title' | 'hasSubCategories' | 'id' | 'parentId' | 'link' | 'subCategories'
  > {}

export const CategoryItem: React.FC<CategoryItemProps> = ({
  title,
  hasSubCategories,
  id,
  parentId,
  link,
  subCategories,
}) => {
  const user = useAppSelector((state) => state.user.user)

  const {
    getIsChecked,
    getIsExpanded,
    toggleChecked,
    toggleExpanded,
  } = useContext(CategoriesContext)

  const toggleExpand = useCallback(
    () => toggleExpanded({ id, subCategories }),
    [getIsExpanded, id]
  )

  const toggleCheck = useCallback(() => toggleChecked({ id, subCategories }), [
    getIsChecked,
    id,
  ])

  const stopPropagation = useCallback<React.EventHandler<any>>(
    (e) => e.stopPropagation(),
    []
  )

  // eslint-disable-next-line no-nested-ternary
  return hasSubCategories ? (
    <Accordion
      className="subCat"
      data-id={id}
      expanded={getIsExpanded(id)}
      onChange={toggleExpand}
    >
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-label="Expand"
        aria-controls={`additional-actions${id}-content`}
        id={`additional-actions${id}-header`}
        onClick={(event) => event.stopPropagation()}
      >
        {!user ? (
          <div>
            <a href={link}>{title}</a>
          </div>
        ) : (
          <FormControlLabel
            aria-label="Acknowledge"
            onClick={stopPropagation}
            onFocus={stopPropagation}
            control={
              <Checkbox
                name={String(id)}
                id={`filter-checkbox-${id}`}
                inputProps={{
                  // @ts-ignore
                  'data-parentid': String(parentId),
                  'data-class': 'filter-checkbox',
                }}
                onChange={toggleCheck}
                value={id}
                checked={getIsChecked(id)}
              />
            }
            label={title}
            id={`filter-label-${id}`}
          />
        )}
      </AccordionSummary>

      {subCategories && getIsExpanded(id) && (
        <AccordionDetails className="details">
          {subCategories.map((item) => (
            <CategoryItem
              key={item.id}
              hasSubCategories={item.hasSubCategories}
              title={item.title}
              id={item.id}
              parentId={item.parentId}
              link={item.link}
              subCategories={item.subCategories}
            />
          ))}
        </AccordionDetails>
      )}
    </Accordion>
  ) : user ? (
    <FormControlLabel
      aria-label="Acknowledge"
      control={
        <Checkbox
          name={String(id)}
          id={`filter-checkbox-${id}`}
          inputProps={{
            // @ts-ignore
            'data-parentid': String(parentId),
            'data-class': 'filter-checkbox',
          }}
          onChange={toggleCheck}
          value={id}
          checked={getIsChecked(id)}
        />
      }
      className="label"
      label={title}
      id={`filter-label-${id}`}
    />
  ) : (
    <div className="padding-l-16">
      <a href={link}>{title}</a>
    </div>
  )
}
