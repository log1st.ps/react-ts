import React, { useCallback } from 'react'
import { useAppDispatch, useAppSelector } from '../../hooks/store'
import { ThreadsTableHeader } from './header/ThreadsTableHeader'
import { ThreadsTableThread } from './thread/ThreadsTableThread'
import { setSettingsSelectedThreads } from '../../slices/threads/threadsSlice'
import { Thread } from '../../models/Thread'

export const ThreadsTable: React.FC = () => {
  const threads = useAppSelector((state) => state.threads.data.threads)

  const dispatch = useAppDispatch()
  const selectedThreads = useAppSelector(
    (state) => state.threads.settings.selectedThreads
  )
  const getIsChecked = useCallback(
    (id: Thread['thread_id']) => selectedThreads.includes(id),
    [selectedThreads]
  )

  const getToggleCheck = useCallback(
    (id: Thread['thread_id']) => () => {
      const index = selectedThreads.indexOf(id)

      if (index > -1) {
        const newIds = [...selectedThreads]
        newIds.splice(newIds.indexOf(id))
        dispatch(setSettingsSelectedThreads([...newIds]))
      } else {
        dispatch(setSettingsSelectedThreads([...selectedThreads, id]))
      }
    },
    [selectedThreads]
  )

  return (
    <div id="threads-table">
      <table>
        <ThreadsTableHeader />
        {threads.length && (
          <tbody>
            {threads.map((thread) => (
              <ThreadsTableThread
                key={thread.thread_id}
                thread={thread}
                isChecked={getIsChecked(thread.thread_id)}
                toggleCheck={getToggleCheck(thread.thread_id)}
              />
            ))}
          </tbody>
        )}
      </table>
    </div>
  )
}
