import React, { useCallback } from 'react'
import classNames from 'classnames'
import { useStateModel } from '../../../hooks/useStateModel'
import {
  setFilterSortDirection,
  setFilterSortField,
} from '../../../slices/threads/threadsSlice'

export const ThreadsTableHeader: React.FC = () => {
  const [sortField, , , setSortField] = useStateModel(
    (state) => state.threads.filters.sortField,
    setFilterSortField,
    true
  )
  const [sortDirection, , , setSortDirection] = useStateModel(
    (state) => state.threads.filters.sortDirection,
    setFilterSortDirection,
    true
  )

  const getSortClick = useCallback(
    (field: string) => () => {
      setSortField({
        currentTarget: {
          value:
            field === sortField
              ? sortDirection === 'desc'
                ? null
                : field
              : field,
        },
      })
      setSortDirection({
        currentTarget: {
          value:
            field === sortField
              ? sortDirection === 'desc'
                ? null
                : 'desc'
              : 'asc',
        },
      })
    },
    [setSortField, sortField, setSortDirection, sortDirection]
  )

  return (
    <thead>
      <tr>
        {[
          [
            ['title', 'Заголовок'],
            ['post_date', 'Дата создания'],
            ['organizer_id', 'Орг'],
          ],
          [
            ['shopping_participants_current', 'Складчиков'],
            ['view_count', 'Просмотров'],
          ],
          [['lastPostDate', 'Последнее сообщение']],
        ].map((group, groupIndex) => (
          <th
            className={classNames({
              'stats-heading': groupIndex === 1,
            })}
            key={group.map(([field]) => field).join('')}
          >
            {group.map(([field, label]) => (
              <span
                key={field}
                onClick={getSortClick(field)}
                role="button"
                tabIndex={0}
                className={classNames({
                  [`th-${field}`]: groupIndex === 0,
                  'first-col': groupIndex === 0,
                  [sortDirection as string]: sortField === field,
                })}
              >
                {label}
              </span>
            ))}
          </th>
        ))}
      </tr>
    </thead>
  )
}
