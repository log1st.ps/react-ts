import React, { useContext, useMemo } from 'react'
import classNames from 'classnames'
import { shoppingTypes, Thread, threadStages } from '../../../models/Thread'
import { Username } from '../../username/Username'
import { formatDate } from '../../../services/utils'
import { AppContext } from '../../coreContext/CoreContext'

export interface ThreadsTableThreadProps {
  thread: Thread
  isChecked: boolean
  toggleCheck: () => void
}

export const ThreadsTableThread: React.FC<ThreadsTableThreadProps> = ({
  thread,
  isChecked,
  toggleCheck,
}) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const isCollectSoon = useMemo(() => {
    const currentDate = Date.now()
    const twoDaysAfter = currentDate + 1000 * 3600 * 24 * 2
    return Boolean(
      thread.deadline_shopping &&
        thread.deadline_shopping! * 1000 > currentDate &&
        thread.deadline_shopping! < twoDaysAfter &&
        thread.shopping_stage === 'open'
    )
  }, [thread.shopping_stage, thread.deadline_shopping])

  const participatorsCount = useMemo(
    () =>
      thread.shopping_participants_current ?? thread.participants_count ?? 0,
    [thread.shopping_participants_current, thread.participants_count]
  )

  const isFakeDeleteThread = false

  const { isAdmin } = useContext(AppContext)

  const stageName = useMemo(
    () =>
      threadStages.find(({ name }) => name === thread.shopping_stage)?.nameRu,
    [threadStages, thread.shopping_stage]
  )

  const shoppingType = useMemo(
    () =>
      shoppingTypes[thread.shopping_type_id as keyof typeof shoppingTypes] ||
      shoppingTypes[1],
    [thread.shopping_type_id]
  )

  return (
    <tr
      className={classNames({
        'Thread-active': isChecked,
      })}
      id={`thread_${thread.thread_id}`}
    >
      <td className="discussionListItem">
        <div
          className={classNames({
            title: true,
            'user-title': isAdmin,
            muted: isFakeDeleteThread,
          })}
        >
          {isAdmin && (
            <input
              className="threadCheckbox"
              checked={isChecked}
              type="checkbox"
              onChange={toggleCheck}
            />
          )}
          {thread.shopping_stage && (
            <span
              className={classNames({
                prefix: true,
                badge: true,
                [thread.shopping_stage]: true,
              })}
            >
              {stageName}
            </span>
          )}
          {isFakeDeleteThread ? (
            <span className="title">{thread.title}</span>
          ) : (
            <>
              <a
                href={thread.thread_link}
                target="_blank"
                rel="noopener noreferrer"
                className="title"
              >
                {thread.title}
              </a>
              {isCollectSoon && (
                <span className="node-title">{thread.node_title}</span>
              )}
            </>
          )}
        </div>
        <div className="badge__footer">
          {isFakeDeleteThread ? (
            <div className="flex justify-space-between">
              <span className="deleted">
                Тема, созданная пользователем
                <Username
                  id={thread.user_id}
                  username={thread.username}
                  url={thread.user_link}
                  className="fs-11"
                />
                , была удалена.
              </span>
              <a
                className="view-deleted"
                href={thread.thread_link}
                target="_blank"
                rel="noopener noreferrer"
              >
                Посмотреть
              </a>
            </div>
          ) : (
            <>
              {isAdmin && (
                <div className="shoppingTypeWrap">
                  <img
                    src={shoppingType.icon}
                    alt={shoppingType.text}
                    className="shoppingTypeIcon"
                  />
                  <span className="shoppingTypeTooltip">
                    {shoppingType.text}
                  </span>
                </div>
              )}
              <span className="muted">
                <Username
                  id={thread.user_id}
                  className="fs-11"
                  username={thread.username}
                  url={thread.user_link}
                />
                ,
              </span>
              <span>
                <a
                  href={thread.thread_link}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="faint fs-11"
                >
                  {formatDate(thread.post_date * 1000)}
                </a>
                ,
              </span>
              <span className="payment fs-11">
                Взнос: {thread.optimized_payment || 0} р.
              </span>
              {isCollectSoon && (
                <>
                  ,{' '}
                  <span className="deadlineShopping fs-11">
                    Сбор взносов {formatDate(thread.deadline_shopping! * 1000)}
                  </span>
                </>
              )}
            </>
          )}
          <div className="iconKey">
            {thread.icon_keys &&
              thread.icon_keys.length > 0 &&
              thread.icon_keys.map((icon) => (
                <span
                  key={icon.text}
                  className={`${icon.className}`}
                  title={`${icon.text}`}
                >
                  {icon.text}
                </span>
              ))}
          </div>
        </div>
      </td>
      <td
        className={classNames({
          stats: true,
          deleted: isFakeDeleteThread,
        })}
      >
        {!isFakeDeleteThread && (
          <>
            <span className="participant">Складчиков: </span>
            <span className="count">{participatorsCount}</span>
            <br />
            <span className="views fs-11">Просмотров: </span>
            <span className="count fs-11">{thread.view_count}</span>
          </>
        )}
      </td>
      <td
        className={classNames({
          'last-post': true,
          deleted: isFakeDeleteThread,
        })}
      >
        {!isFakeDeleteThread && (
          <>
            <Username
              id={thread.last_post_user_id}
              url={thread.last_post_user_link}
            >
              <img
                src={thread.last_post_user_avatar}
                alt={thread.last_post_username}
              />
              <div
                dangerouslySetInnerHTML={{
                  __html: thread.last_post_user_style,
                }}
              />
            </Username>
            <a
              href={thread.last_post_link}
              target="_blank"
              rel="noopener noreferrer"
              className="last-post-date muted"
            >
              {formatDate(thread.last_post_date * 1000)}
            </a>
          </>
        )}
      </td>
    </tr>
  )
}
