import { createContext } from 'react'

export const CoreContext = createContext<{
  rootNode: number | null
  xfSession: string | null
}>({
  rootNode: null,
  xfSession: null,
})

export const AppContext = createContext<{
  isAuthorized: boolean
  isAdmin: boolean
  userDataId: number | null
  setUserDataId: (id: number | null) => void
}>({
  isAuthorized: false,
  isAdmin: false,
  userDataId: null,
  setUserDataId: () => {},
})
