import React from 'react'
import { useAppSelector } from '../../hooks/store'
import { formatNum } from '../../services/utils'
import { Username } from '../username/Username'

function BoardTotals() {
  const boardTotals = useAppSelector((state) => state.sidebar.boardTotals)

  return (
    <>
      <div className="section">
        <div className="secondaryContent">
          <h3>Статистика форума</h3>
          <div className="pairsJustified">
            <dl>
              <dt>Темы:</dt>
              <dd>{formatNum(boardTotals.discussions)}</dd>
            </dl>
            <dl>
              <dt>Сообщений:</dt>
              <dd>{formatNum(boardTotals.messages)}</dd>
            </dl>
            <dl>
              <dt>Пользователи:</dt>
              <dd>{formatNum(boardTotals.users)}</dd>
            </dl>
            {boardTotals.latestUser && (
              <dl>
                <dt>Новый пользователь:</dt>
                <dd>
                  <Username
                    username={boardTotals.latestUser.username}
                    id={boardTotals.latestUser.user_id}
                  />
                </dd>
              </dl>
            )}
          </div>
        </div>
      </div>
    </>
  )
}

export default BoardTotals
