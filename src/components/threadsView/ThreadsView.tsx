import React, { memo, useContext, useEffect } from 'react'
import { useAppDispatch, useAppSelector } from '../../hooks/store'
import {
  fetchThreads,
  ThreadsDisplayType,
} from '../../slices/threads/threadsSlice'
import { ThreadsList } from '../threadsList/ThreadsList'
import { ThreadsTable } from '../threadsTable/ThreadsTable'
import { CoreContext } from '../coreContext/CoreContext'
import Loader from '../loader/Loader'

export const ThreadsView: React.FC = memo(() => {
  const isLoading = useAppSelector((state) => state.threads.isLoading)
  const hasThreads = useAppSelector(
    (state) => !!state.threads.data.threads.length
  )
  const displayType = useAppSelector(
    (state) => state.threads.settings.displayType
  )

  const dispatch = useAppDispatch()
  const { rootNode } = useContext(CoreContext)

  const filters = useAppSelector((state) => state.threads.filters)

  useEffect(() => {
    dispatch(fetchThreads({ parentId: rootNode }))
  }, [filters])

  return (
    <div className="position-relative">
      {hasThreads ? (
        <div className="sectionMain">
          {displayType === ThreadsDisplayType.table ? (
            <ThreadsTable />
          ) : (
            <ThreadsList />
          )}
        </div>
      ) : (
        !isLoading && (
          <div className="sectionMain">
            <p>Ничего не найдено</p>
          </div>
        )
      )}
      {isLoading && <Loader />}
    </div>
  )
})
