import React from 'react'

export const SupportLinks: React.FC = () => (
  <div className="section avatarList">
    <div className="secondaryContent">
      <h3>
        <img
          src="styles/Skladchik1/xenforo/ticket-h3-icon.png"
          alt=""
          className="ticket_icon"
        />
        Служба поддержки
      </h3>
      <ul>
        <li>
          <a href="support-tickets/open">Задать вопрос</a>
        </li>
        <li>
          <a href="support-tickets">Мои вопросы</a>
        </li>
        <li>
          <a href="support-tickets/list">Все тикеты</a>
        </li>
        <li>
          <a href="support-tickets/list?ticket_active=1">Активные тикеты</a>
        </li>
        <li>
          <a href="support-tickets/list?awating_reply=1">
            Тикеты в ожидании ответа
          </a>
        </li>
        <li>
          <a href="support-tickets/departments">Список отделов</a>
        </li>
      </ul>
    </div>
  </div>
)
