import React from 'react'
import { useAppSelector } from '../../hooks/store'
import { Username } from '../username/Username'

export const OnlineStaff: React.FC = () => {
  const onlineStaff = useAppSelector((state) => state.sidebar.onlineStaff)

  return (
    <div className="section  avatarList team_forum_online">
      <div className="secondaryContent">
        <h3>
          <a href="/members/?type=staff">Команда форума в сети</a>
        </h3>
        <ul>
          {onlineStaff.records.map((record) => (
            <li key={record.user_id}>
              <Username id={record.user_id}>
                <div className="avatar">
                  <img src={record.avatar} alt={record.username} />
                </div>
                {/* eslint-disable-next-line react/no-danger */}
                <div dangerouslySetInnerHTML={{ __html: record.user_style }} />
                <div className="userTitle">{record.userTitle}</div>
              </Username>
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}
