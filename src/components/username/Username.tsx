import React, { useCallback, useContext } from 'react'
import classNames from 'classnames'
import { User } from '../../models/User'
import { AppContext } from '../coreContext/CoreContext'

export interface UsernameProps {
  username?: User['username']
  id: User['user_id']
  className?: string
  url?: string
}

export const Username: React.FC<UsernameProps> = ({
  username,
  id,
  className,
  url = '#',
  children,
}) => {
  const { setUserDataId } = useContext(AppContext)
  const onClick = useCallback(
    (e: React.MouseEvent) => {
      e.preventDefault()
      setUserDataId(id)
    },
    [id]
  )

  return (
    <a
      href={url}
      className={classNames(['username', className])}
      onClick={onClick}
    >
      {children || username}
    </a>
  )
}
