import React, { createContext, useContext, useEffect } from 'react'
import { Checkbox, FormControlLabel, FormLabel } from '@material-ui/core'
import { isEqual, sortBy } from 'lodash'
import { fetchCategories } from '../../slices/categories/categoriesSlice'
import { useAppDispatch, useAppSelector } from '../../hooks/store'
import { CoreContext } from '../coreContext/CoreContext'
import { useToggle } from '../../hooks/useToggle'
import { CategoryItem } from '../categoryItem/CategoryItem'
import { Category } from '../../models/Category'
import { useCategoriesToggle } from '../../hooks/useCategoriesToggle'
import { setFilterCategories } from '../../slices/threads/threadsSlice'
import Loader from '../loader/Loader'

export interface CategoriesContextState {
  getIsChecked: (id: Category['id']) => boolean
  toggleChecked: (category: {
    id: Category['id']
    subCategories: Category['subCategories']
  }) => void
  getIsExpanded: (id: Category['id']) => boolean
  toggleExpanded: (category: {
    id: Category['id']
    subCategories: Category['subCategories']
  }) => void
}

export const CategoriesContext = createContext<CategoriesContextState>({
  getIsChecked: () => false,
  toggleChecked: () => {},
  getIsExpanded: () => false,
  toggleExpanded: () => {},
})

const Categories: React.FC = () => {
  const dispatch = useAppDispatch()

  const isLoading = useAppSelector((state) => state.categories.isLoading)

  const { rootNode } = useContext(CoreContext)
  useEffect(() => {
    dispatch(fetchCategories({ parentId: rootNode }))
  }, [fetchCategories, dispatch, rootNode])

  const permissions = useAppSelector((state) => state.user.user?.permissions)

  const [autoSelectSubcategories, toggleAutoSelectSubcategories] = useToggle(
    true
  )

  const categories = useAppSelector((state) => state.categories.categories)

  const selectedCategories = useAppSelector(
    (state) => state.threads.filters.selectedCategories
  )

  const [, getIsExpanded, toggleExpanded] = useCategoriesToggle(
    [],
    autoSelectSubcategories
  )
  const [checked, getIsChecked, toggleChecked] = useCategoriesToggle(
    [...selectedCategories],
    autoSelectSubcategories
  )

  const filterCategories = useAppSelector(
    (state) => state.threads.filters.selectedCategories
  )

  useEffect(() => {
    if (isEqual(sortBy(filterCategories), sortBy(checked))) {
      return
    }
    dispatch(setFilterCategories([...checked]))
  }, [checked])

  return (
    <div className="filter-block filter-block__categories position-relative">
      {isLoading && <Loader />}
      <FormLabel
        component="legend"
        disabled
        className="margin-bottom-10 fb_c__legend"
      >
        Категории
      </FormLabel>
      {permissions?.general?.canSeeModerFilters && (
        <FormControlLabel
          aria-label="Acknowledge"
          control={
            <Checkbox
              name="checkSubcategoriesToggle"
              onChange={toggleAutoSelectSubcategories}
              id="category-filter-checksubcategories-toggle"
              checked={autoSelectSubcategories}
            />
          }
          className="label"
          label="Выбирать дочерние категории"
        />
      )}
      <CategoriesContext.Provider
        value={{ getIsChecked, toggleChecked, getIsExpanded, toggleExpanded }}
      >
        {categories.map((category) => (
          <CategoryItem
            key={category.id}
            hasSubCategories={category.hasSubCategories}
            title={category.title}
            id={category.id}
            parentId={category.parentId}
            link={category.link}
            subCategories={category.subCategories}
          />
        ))}
      </CategoriesContext.Provider>
    </div>
  )
}

export default Categories
