import React, { useContext } from 'react'
import Categories from '../../slices/categories'
import Sidebar from '../../slices/sidebar'
import { useAppSelector } from '../../hooks/store'
import { Online15 } from '../online15/Online15'
import { Online60 } from '../online60/Online60'
import { OnlineStaff } from '../onlineStaff/OnlineStaff'
import { Filters } from '../filters/Filters'
import { AppContext } from '../coreContext/CoreContext'

export interface RightSideProps {
  id: string
}

export const RightSide: React.FC<RightSideProps> = ({ id }) => {
  const { onlineStaff, online15, online60 } = useAppSelector(
    (state) => state.sidebar
  )

  const user = useAppSelector((state) => state.user.user)

  const { isAdmin } = useContext(AppContext)

  return (
    <div id={id} className="sidebar relative">
      {user && <Filters />}
      <Categories />
      <Sidebar />
      {!!onlineStaff.records.length && <OnlineStaff />}
      {isAdmin && !!online15.records.length && <Online15 />}
      {!!online60.records.length && <Online60 />}
    </div>
  )
}
