import React from 'react'
import classNames from 'classnames'
import { useAppSelector } from '../../hooks/store'
import { ThreadsDisplayType } from '../../slices/threads/threadsSlice'
import { formatDate } from '../../services/utils'
import { Thread } from '../../models/Thread'

const isEmpty = (thread: Thread) => !(thread.post_image || thread.message_html)

export const ThreadsList: React.FC = () => {
  const threads = useAppSelector((state) => state.threads.data.threads)
  const displayType = useAppSelector(
    (state) => state.threads.settings.displayType
  )

  return (
    <ol
      id="messageList"
      className={classNames({
        messageList: true,
        [`messageList--${displayType}`]: true,
      })}
    >
      {threads.map((thread) => (
        <li
          key={thread.thread_id}
          className={
            displayType === ThreadsDisplayType.grid
              ? 'grid-message'
              : !isEmpty(thread)
              ? 'message'
              : 'message--closed'
          }
        >
          <h3 className="title slist icon_arrow_theme">
            <a href={thread.thread_link} target="_blank" rel="noreferrer">
              {thread.title}
            </a>
          </h3>
          <div className="nodeLastPost">
            <span>
              <img
                alt={thread.title}
                src="/styles/esthetic/cs/icon_post_target.gif"
              />
            </span>
            <span className="lastThreadUser">
              <a href={thread.user_link} className="username">
                {thread.username}
              </a>
              <span> » </span>
              <span>{formatDate(thread.post_date * 1000)}</span>
            </span>
          </div>
          {!isEmpty(thread) && (
            <div className="messageContent">
              {thread.post_image && (
                <div className="text-center">
                  <a
                    href={thread.thread_link}
                    title={thread.title}
                    target="_blank"
                    rel="noreferrer"
                  >
                    <img
                      src={thread.post_image}
                      alt={thread.title}
                      className="post-img"
                    />
                  </a>
                </div>
              )}
              <article>
                <blockquote
                  className="messageText ugc baseHtml"
                  dangerouslySetInnerHTML={{ __html: thread.message_html }}
                />
              </article>
            </div>
          )}
        </li>
      ))}
    </ol>
  )
}
