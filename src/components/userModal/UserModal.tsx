import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react'
import { Modal } from '@material-ui/core'
import { AppContext, CoreContext } from '../coreContext/CoreContext'
import { UserData } from '../../models/User'
import { useAppDispatch } from '../../hooks/store'
import { fetchUserData } from '../../slices/user/userSlice'
import { formatDate } from '../../services/utils'

export const UserModal: React.FC = () => {
  const { xfSession } = useContext(CoreContext)
  const { userDataId, setUserDataId, isAdmin } = useContext(AppContext)

  const [userData, setUserData] = useState<UserData | null>(null)

  const dispatch = useAppDispatch()

  useEffect(() => {
    if (userDataId) {
      dispatch(fetchUserData({ id: userDataId })).then(({ payload }) => {
        setUserData(payload as UserData)
      })
      return
    }
    setUserData(null)
  }, [userDataId])

  const close = useCallback(() => {
    setUserDataId(null)
  }, [setUserDataId])

  const userKey = useMemo(
    () => `${userData?.user.username}.${userData?.user.user_id}`,
    [userData]
  )

  const stopPropagation = (e: React.MouseEvent) => e.stopPropagation()

  return (
    <Modal
      open={!!userData}
      onClose={close}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      disableScrollLock
    >
      {userData ? (
        <div id="user-modal-body" className="xenOverlay">
          <a role="button" tabIndex={0} className="close" onClick={close} />
          <div className="img-cropper">
            <img
              src={userData.user.avatar}
              alt={userData.user.username}
              style={{
                bottom: userData.user.avatar_crop_y * 2,
                right: userData.user.avatar_crop_x * 2,
                position: 'relative',
              }}
            />
            {isAdmin && (
              <div className="img-cropper__tools">
                <a
                  href={`/members/${userKey}/edit`}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="link"
                  onClick={stopPropagation}
                >
                  Редактирование
                </a>
                <a
                  href={`/spam-cleaner/${userKey}/`}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="link"
                  onClick={stopPropagation}
                >
                  Спам
                </a>
                <a
                  href={`/members/${userKey}/warn `}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="link"
                  onClick={stopPropagation}
                >
                  Предупредить
                </a>
                <a
                  href={`/fxaadmin42c.php?banning/users/${userKey}/add`}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="link"
                  onClick={stopPropagation}
                >
                  Забанить
                </a>
              </div>
            )}
          </div>
          <div className="description">
            <h3>
              <a
                href={userData.user.link}
                target="_blank"
                rel="noopener noreferrer"
              >
                {userData.user.username}
              </a>
              {userData.user.hasMaster && (
                <img
                  src={`https://s4.skladchik.ws${userData.user.hasMaster}`}
                  alt=""
                />
              )}
              {userData.user.userSklad && (
                <a
                  href={userData.user.userSklad.userLink}
                  target="_blank"
                  rel="noreferrer"
                  className="user-modal-body__user-link"
                >
                  {userData.user.userSklad.username}
                </a>
              )}
            </h3>

            <h4 className="userTitle">{userData.user.userTitle}</h4>
            <div className="userBlurb">
              {[
                ({ male: 'Мужской', female: 'Женский' } as any)[
                  userData.user.gender
                ],
                userData.user.age,
              ]
                .filter(Boolean)
                .join(', ')}
            </div>
            <div className="userStatus">{userData.user.status}</div>
            <div className="userLinks">
              <a
                href={userData.user.link}
                target="_blank"
                rel="noopener noreferrer"
              >
                Страница профиля
              </a>
              <a
                href={`/dialogs/?to=${userData.user.user_id}`}
                target="_blank"
                rel="noopener noreferrer"
              >
                Начать переписку
              </a>
              <a
                href={`${userData.user.link}follow?xfToken=${xfSession}`}
                target="_blank"
                rel="noopener noreferrer"
                className="relative subscribe"
              >
                Подписаться
              </a>
              <a
                href={`${userData.user.link}ignore`}
                target="_blank"
                rel="noopener noreferrer"
              >
                Игнорировать
              </a>
              {isAdmin && (
                <a
                  href={`/misc/do-login-as-user?username=${userData.user.username}`}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Switch User
                </a>
              )}
            </div>
            <div className="userStats">
              Оплачено:{' '}
              <a
                href={`${userData.user.link}list-joined-for-some-groups`}
                target="_blank"
                rel="noopener noreferrer"
              >
                {userData.user.payment_count}
              </a>
              Организовано складчин:
              {isAdmin ? (
                <>
                  <a
                    href={`${userData.user.link}list-organized-admin?type=dostupno`}
                    target="_blank"
                    rel="noopener noreferrer"
                    className="slash"
                  >
                    {' '}
                    {userData.user.count_org_finished_thread}
                  </a>
                  <span className="slash"> / </span>
                  <a
                    href={`${userData.user.link}list-organized-admin`}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {userData.user.organized_count}
                  </a>
                </>
              ) : (
                <span>{userData.user.organized_count}</span>
              )}
              На форуме с:{' '}
              <span>{formatDate(userData.user.register_date * 1000)}</span>
              Сообщений:{' '}
              <a
                href={`/search/member?user_id=${userData.user.user_id}`}
                target="_blank"
                rel="noopener noreferrer"
              >
                {userData.user.message_count}
              </a>
              Симпатии: <span>{userData.user.like_count}</span>
              Баллы:{' '}
              <a
                href={`${userData.user.link}trophies`}
                target="_blank"
                rel="noopener noreferrer"
              >
                {userData.user.trophy_points}
              </a>
              {isAdmin && (
                <>
                  Баллы за нарушения:{' '}
                  <a
                    href={`${userData.user.link}#warnings`}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {userData.user.warning_points}
                  </a>
                </>
              )}
            </div>
            <div
              className="lastActivity"
              dangerouslySetInnerHTML={{ __html: userData.user.lastActivity }}
            />
          </div>
        </div>
      ) : (
        <></>
      )}
    </Modal>
  )
}
