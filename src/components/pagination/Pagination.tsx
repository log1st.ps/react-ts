import React, {
  ChangeEvent,
  useCallback,
  createContext,
  useContext,
  MouseEvent,
} from 'react'
import {
  Pagination as RootPagination,
  PaginationItemProps,
} from '@material-ui/lab'

import classNames from 'classnames'
import { useStateModel } from '../../hooks/useStateModel'
import { useAppSelector } from '../../hooks/store'
import { setFilterPage } from '../../slices/threads/threadsSlice'

const PaginationContext = createContext<{
  page: number
  totalPages: number
}>({
  page: 0,
  totalPages: 0,
})

const PaginationItem: React.FC<PaginationItemProps> = ({
  page,
  type,
  onClick,
  selected,
}) => {
  if (['start-ellipsis', 'end-ellipsis'].includes(type!)) {
    return <span className="pagination__ellipsis">...</span>
  }

  const { totalPages } = useContext(PaginationContext)

  if (page === 0 && type === 'previous') {
    return null
  }
  if (page === totalPages + 1 && type === 'next') {
    return null
  }

  const handler = (e: MouseEvent) => {
    onClick?.(e as MouseEvent<HTMLDivElement>)
  }

  return (
    <button
      className={classNames({
        pagination__btn: true,
        'pagination__btn--active': selected,
      })}
      onClick={handler}
      type="button"
    >
      {type === 'page'
        ? page
        : type === 'previous'
        ? '< Назад'
        : type === 'next'
        ? 'Вперёд >'
        : '-'}
    </button>
  )
}

export const Pagination: React.FC = () => {
  const totalPages = useAppSelector((state) => state.threads.data.totalPages)
  const [page, setPage] = useStateModel(
    (state) => state.threads.data.page,
    setFilterPage,
    true
  )
  const onPageChange = useCallback(
    (e: ChangeEvent<any>, newPage: number) => {
      setPage(newPage)
    },
    [setPage]
  )

  return totalPages ? (
    <div className="PageNav pagination">
      <span>
        Страница {page} из {totalPages}
      </span>
      <PaginationContext.Provider value={{ page, totalPages }}>
        <RootPagination
          renderItem={PaginationItem}
          disabled={false}
          count={totalPages}
          defaultPage={page || 1}
          page={page}
          siblingCount={2}
          boundaryCount={1}
          onChange={onPageChange}
        />
      </PaginationContext.Provider>
    </div>
  ) : null
}
