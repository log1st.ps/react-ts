import React from 'react'
import { useAppSelector } from '../../hooks/store'
import { Online } from '../online/Online'

export const Online15: React.FC = () => {
  const online15 = useAppSelector((state) => state.sidebar.online15)

  return <Online data={online15} />
}
