import React, { ChangeEvent, useCallback } from 'react'
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
} from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import HighlightOffIcon from '@material-ui/icons/HighlightOff'
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers'
import DateFnsUtils from '@date-io/date-fns'
import ruLocale from 'date-fns/locale/ru'
import { useAppSelector } from '../../hooks/store'
import { useStateModel } from '../../hooks/useStateModel'
import {
  setFilterDeleted,
  setFilterFrom,
  setFilterImgPreview,
  setFilterIsOrg,
  setFilterIsPart,
  setFilterSortThreads,
  setFilterStages,
  setFilterText,
  setFilterTo,
} from '../../slices/threads/threadsSlice'
import { threadStages } from '../../models/Thread'

export const Filters: React.FC = () => {
  const permissions = useAppSelector((state) => state.user.user?.permissions)

  const [text, setText, emitText, onTextChange] = useStateModel<string>(
    (state) => state.threads.filters.text,
    setFilterText,
    false,
    undefined,
    ''
  )
  const onClearTextBtnClick = useCallback(() => {
    setText('', true)
  }, [setText])

  const [dateFrom, , , onDateFromChange, onClearDateFrom] = useStateModel(
    (state) => state.threads.filters.from,
    setFilterFrom,
    true,
    (value: Date | null) => value?.getTime() || null
  )

  const [dateTo, , , onDateToChange, onClearDateTo] = useStateModel(
    (state) => state.threads.filters.to,
    setFilterTo,
    true,
    (value: Date | null) => value?.getTime() || null
  )

  const [isOrg, , , onIsOrgChange] = useStateModel(
    (state) => state.threads.filters.isOrg,
    setFilterIsOrg,
    true,
    (value) => +value as 0 | 1 | 2
  )

  const [isPart, , , onIsPartChange] = useStateModel(
    (state) => state.threads.filters.isPart,
    setFilterIsPart,
    true,
    (value) => +value as 0 | 1 | 2
  )

  const [imgPreview, , , onImgPreviewChange] = useStateModel(
    (state) => state.threads.filters.imgPreview,
    setFilterImgPreview,
    true,
    (value) => +value as 0 | 1 | 2
  )

  const [deleted, , , onDeletedChange] = useStateModel(
    (state) => state.threads.filters.deleted,
    setFilterDeleted,
    true,
    (value) => +value as 0 | 1 | 2 | 3
  )

  const [sortThreads, , , onSortThreadsChange] = useStateModel(
    (state) => state.threads.filters.sortThreads,
    setFilterSortThreads,
    true,
    (value) => value as 'asc' | 'desc'
  )

  const [stages, , , onStagesChange] = useStateModel(
    (state) => state.threads.filters.stages,
    setFilterStages,
    true,
    (e: number): number[] => {
      const value = +e
      const index = stages.indexOf(value)
      if (index > -1) {
        const newStages = [...stages]
        newStages.splice(index, 1)
        return [...newStages]
      }
      return [...stages, value]
    }
  )

  return (
    <div className="filter-block filter-block__filters section">
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel8a-content"
          id="panel8a-header"
        >
          <FormLabel component="legend" disabled>
            Фильтры
          </FormLabel>
        </AccordionSummary>
        <AccordionDetails>
          <div className="checkboxes-wrap">
            {!permissions?.general.canUseKeywordFilters && (
              <FormControl component="fieldset" className="w-100 margin-top-20">
                <FormLabel component="legend" disabled>
                  Ключевое слово
                </FormLabel>
                <div className="relative">
                  <input
                    type="text"
                    name="keyWord"
                    onChange={onTextChange}
                    id="category-filter-keyword"
                    value={text}
                  />
                  <a href="#" onClick={emitText} className="callToAction">
                    <span>ОК</span>
                  </a>
                  {text && (
                    <span
                      role="button"
                      tabIndex={0}
                      className="clear-keyword-btn"
                      onClick={onClearTextBtnClick}
                    >
                      <HighlightOffIcon />
                    </span>
                  )}
                </div>
              </FormControl>
            )}
            <FormControl className="w-100 margin-top-20">
              <FormLabel component="legend" disabled>
                Дата создания
              </FormLabel>
              <MuiPickersUtilsProvider utils={DateFnsUtils} locale={ruLocale}>
                {([
                  [dateFrom, onDateFromChange, onClearDateFrom, 'from', 'От'],
                  [dateTo, onDateToChange, onClearDateTo, 'to', 'До'],
                ] as Array<
                  [
                    number | null,
                    (d: Date | null) => void,
                    () => void,
                    string,
                    string
                  ]
                >).map(([val, setVal, clearVal, key, label]) => (
                  <div className="datepicker-wrap" key={key}>
                    <KeyboardDatePicker
                      className={`date-${key}`}
                      disableToolbar
                      autoOk
                      id={`category-filter-date-${key}`}
                      format="dd-MM-yyyy"
                      value={val}
                      variant="inline"
                      onChange={setVal}
                      views={['year', 'month', 'date']}
                      label={label}
                      maxDate={new Date('01-01-2080')}
                      minDate={new Date('01-01-2008')}
                      minDateMessage="Дата меньше чем возможно"
                      maxDateMessage="Дата больше чем возможно"
                      invalidDateMessage="Неверный формат даты"
                      PopoverProps={{
                        className: 'datepicker-popover',
                        disableScrollLock: true,
                      }}
                      InputLabelProps={{ shrink: true }}
                    />
                    {val && (
                      <span
                        role="button"
                        tabIndex={0}
                        className="clear-btn"
                        onClick={clearVal}
                      >
                        <HighlightOffIcon />
                      </span>
                    )}
                  </div>
                ))}
              </MuiPickersUtilsProvider>
            </FormControl>
            {([
              !permissions?.general.canSeeModerFilters && [
                'Наличие орга',
                'org',
                isOrg,
                onIsOrgChange,
                [
                  [0, 'Показать все темы'],
                  [1, 'Темы с организатором'],
                  [2, 'Темы без организатора'],
                ],
              ],
              [
                'Есть участники',
                'part',
                isPart,
                onIsPartChange,
                [
                  [0, 'все'],
                  [1, 'да'],
                  [2, 'нет'],
                ],
              ],
              !permissions?.general.canSeeModerFilters && [
                'Есть превью картинки',
                'img-preview',
                imgPreview,
                onImgPreviewChange,
                [
                  [0, 'все'],
                  [1, 'да'],
                  [2, 'нет'],
                ],
              ],
              !permissions?.general.canSeeModerFilters && [
                'Удаленные',
                'deleted',
                deleted,
                onDeletedChange,
                [
                  [0, 'Только активные'],
                  [1, 'Удаленные'],
                  [2, 'Не удаленные'],
                  [3, 'Все'],
                ],
              ],
              permissions?.general.canSeeModerFilters && [
                'Сортировка',
                'sortThreads',
                sortThreads,
                onSortThreadsChange,
                [
                  ['desc', 'Выше где больше участников'],
                  ['asc', 'Выше где меньше участников'],
                ],
              ],
            ].filter(Boolean) as Array<
              [
                string,
                string,
                number,
                (d: ChangeEvent<HTMLInputElement>) => void,
                Array<[number, string]>
              ]
            >).map(([label, id, val, setVal, options]) => (
              <FormControl
                key={id}
                component="fieldset"
                className="w-100 margin-top-20"
              >
                <FormLabel component="legend" disabled>
                  {label}
                </FormLabel>
                <RadioGroup
                  aria-label={id}
                  name={id}
                  id={`category-filter-${id}`}
                  className="color-blue"
                  value={val}
                  onChange={setVal}
                >
                  {options.map(([v, l]) => (
                    <FormControlLabel
                      value={v}
                      control={<Radio />}
                      label={l}
                      key={v}
                    />
                  ))}
                </RadioGroup>
              </FormControl>
            ))}
            <FormControl
              component="fieldset"
              className="w-100 margin-top-20 stages"
            >
              <FormLabel component="legend" disabled>
                Этапы
              </FormLabel>
              {threadStages.map(({ id, nameRu }) => (
                <FormControlLabel
                  key={id}
                  aria-label="Acknowledge"
                  control={
                    <Checkbox
                      name="checkCollectSoon"
                      onChange={onStagesChange}
                      id={`category-filter-${id}`}
                      checked={stages.includes(id)}
                      value={id}
                    />
                  }
                  className="label"
                  label={nameRu}
                />
              ))}
            </FormControl>
          </div>
        </AccordionDetails>
      </Accordion>
    </div>
  )
}
