import React from 'react'
import { useAppSelector } from '../../hooks/store'
import { Online } from '../online/Online'

export const Online60: React.FC = () => {
  const online60 = useAppSelector((state) => state.sidebar.online60)

  return <Online data={online60} />
}
