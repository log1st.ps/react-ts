import React from 'react'
import classNames from 'classnames'
import { useStateModel } from '../../hooks/useStateModel'
import {
  setSettingsDisplayType,
  ThreadsDisplayType,
} from '../../slices/threads/threadsSlice'
import { ReactComponent as TableIcon } from '../../assets/table.svg'
import { ReactComponent as ListIcon } from '../../assets/list.svg'
import { ReactComponent as GridIcon } from '../../assets/grid.svg'

export const DisplayTypeSelector: React.FC = () => {
  const [displayType, setDisplayType] = useStateModel(
    (state) => state.threads.settings.displayType,
    setSettingsDisplayType,
    true
  )

  const iconsMap = {
    [ThreadsDisplayType.table]: <TableIcon />,
    [ThreadsDisplayType.grid]: <GridIcon />,
    [ThreadsDisplayType.list]: <ListIcon />,
  }

  return (
    <div className="btn-view-wrap">
      {[
        ThreadsDisplayType.list,
        ThreadsDisplayType.grid,
        ThreadsDisplayType.table,
      ].map((type) => (
        <a
          className={classNames({
            'btn-view': true,
            active: type === displayType,
          })}
          id={`${type}-view`}
          onClick={() => setDisplayType(type)}
          role="button"
          tabIndex={0}
          key={type}
        >
          {iconsMap[type]}
        </a>
      ))}
    </div>
  )
}
