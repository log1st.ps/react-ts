import React, { useContext, useEffect, useState } from 'react'
import { RightSide } from './components/rightSide/RightSide'
import { fetchUser } from './slices/user/userSlice'
import { useAppDispatch, useAppSelector } from './hooks/store'
import { AppContext, CoreContext } from './components/coreContext/CoreContext'

import { Pagination } from './components/pagination/Pagination'
import { MakeThread } from './components/makeThread/MakeThread'
import { DisplayTypeSelector } from './components/displayTypeSelector/DisplayTypeSelector'

import './App.scss'
import 'tree_select/dist/index.css'
import { ThreadsView } from './components/threadsView/ThreadsView'
import { StagesTabs } from './components/stagesTabs/StagesTabs'
import { isAdmin } from './services/utils'
import { UserModal } from './components/userModal/UserModal'

const App: React.FC = () => {
  const dispatch = useAppDispatch()
  const { xfSession } = useContext(CoreContext)

  useEffect(() => {
    dispatch(fetchUser({ xfSession }))
  }, [xfSession, fetchUser, dispatch])

  const isAuthorized = useAppSelector((state) => !!state.user.user)
  const hasAdminPermissions = useAppSelector((state) =>
    isAdmin(state.user.user?.permissions)
  )

  const [userDataId, setUserDataId] = useState<number | null>(null)

  return (
    <AppContext.Provider
      value={{
        isAuthorized,
        isAdmin: hasAdminPermissions,
        userDataId,
        setUserDataId,
      }}
    >
      <div className="clear-app__wrapper clear-app__wrapper--center">
        <div className="clear-app__content">
          <StagesTabs />
        </div>
      </div>
      <div className="clear-app__wrapper clear-app__wrapper--center">
        <div className="clear-app__content clear-app__nav">
          <div className="clear-app__pagination">
            <Pagination />
          </div>
          <div className="clear-app__display-types">
            <DisplayTypeSelector />
          </div>
        </div>
        <div className="clear-app__side">
          <MakeThread />
        </div>
      </div>
      <div className="clear-app__wrapper clear-app__wrapper--start">
        <div className="clear-app__content" id="ns_leftcol">
          <ThreadsView />
        </div>
        <div className="clear-app__side">
          <RightSide id="ns_rightcol" />
        </div>
      </div>
      <div className="clear-app-wrapper clear-app__wrapper--start">
        <div className="clear-app__content">
          <div className="clear-app__pagination">
            <Pagination />
          </div>
        </div>
      </div>
      <UserModal />
    </AppContext.Provider>
  )
}

export default App
